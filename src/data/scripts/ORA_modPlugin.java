package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.ids.Skills;
import data.scripts.ai.ORA_invocationAI;
import data.scripts.ai.ORA_callingAI;
import data.scripts.ai.ORA_echoesAI;
import data.scripts.ai.ORA_prayerAI;
import data.scripts.world.ORA_gen;
import exerelin.campaign.SectorManager;
import org.dark.shaders.light.LightData;
import org.dark.shaders.util.ShaderLib;
import org.dark.shaders.util.TextureData;

public class ORA_modPlugin extends BaseModPlugin {
    
    public static final String ECHOES_ID = "ora_echoesS";  
    public static final String INVOCATION_ID = "ora_invocationS";  
    public static final String CALLING_ID = "ora_callingS";  
    public static final String PRAYER_ID = "ora_prayerS";  
    
    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        switch (missile.getProjectileSpecId()) {
            case ECHOES_ID:
                return new PluginPick<MissileAIPlugin>(new ORA_echoesAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case INVOCATION_ID:
                return new PluginPick<MissileAIPlugin>(new ORA_invocationAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case CALLING_ID:
                return new PluginPick<MissileAIPlugin>(new ORA_callingAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case PRAYER_ID:
                return new PluginPick<MissileAIPlugin>(new ORA_prayerAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            default:        
        }
        return null;
    }
    
    private static void initORA() {
        boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        if (!haveNexerelin || SectorManager.getCorvusMode()){
            new ORA_gen().generate(Global.getSector());
            // Exerelin not found so continue and run normal generation code
        }		
    }
    
    @Override
	public void onApplicationLoad() throws ClassNotFoundException {  
            
        try {
            Global.getSettings().getScriptClassLoader().loadClass("org.lazywizard.lazylib.ModUtils");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "LazyLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download LazyLib at http://fractalsoftworks.com/forum/index.php?topic=5444"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }
        
        try {
            Global.getSettings().getScriptClassLoader().loadClass("data.scripts.util.MagicAnim");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "MagicLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download MagicLib at http://fractalsoftworks.com/forum/index.php?topic=13718.0"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }
        
        try {  
            Global.getSettings().getScriptClassLoader().loadClass("org.dark.shaders.util.ShaderLib");  
        } catch (ClassNotFoundException ex) {  
            return;  
        }  
        
        ShaderLib.init();  
        LightData.readLightDataCSV("data/lights/ora_light.csv"); 
        TextureData.readTextureDataCSV("data/lights/ora_texture.csv"); 
    }
	
    @Override
    public void onNewGame() {
        initORA();
    }
    
    @Override
    public void onNewGameAfterEconomyLoad() {
	//special admins
        MarketAPI market =  Global.getSector().getEconomy().getMarket("joy_satiate");
        if (market != null && market.getAdmin()!=null) {
            PersonAPI person = market.getAdmin();
            
            person.getStats().setSkillLevel(Skills.INDUSTRIAL_PLANNING, 0);
            person.getStats().setSkillLevel(Skills.FLEET_LOGISTICS, 3);
            person.getStats().setSkillLevel(Skills.PLANETARY_OPERATIONS, 3);
        }
        
        market =  Global.getSector().getEconomy().getMarket("ora_poincare");
        if (market != null && market.getAdmin()!=null) {
            PersonAPI person = market.getAdmin();
            
            person.getStats().setSkillLevel(Skills.INDUSTRIAL_PLANNING, 0);
            person.getStats().setSkillLevel(Skills.FLEET_LOGISTICS, 3);
            person.getStats().setSkillLevel(Skills.PLANETARY_OPERATIONS, 3);
        }
        
        market =  Global.getSector().getEconomy().getMarket("ora_pendulum");
        if (market != null && market.getAdmin()!=null) {
            PersonAPI person = market.getAdmin();
            
            person.getStats().setSkillLevel(Skills.INDUSTRIAL_PLANNING, 0);
            person.getStats().setSkillLevel(Skills.FLEET_LOGISTICS, 0);
            person.getStats().setSkillLevel(Skills.PLANETARY_OPERATIONS, 3);
        }
    }
}
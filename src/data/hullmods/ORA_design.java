package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unchecked")
public class ORA_design extends BaseHullMod {
    
    private final float SHIELD_ROTATION = 50;
    
    private final static Map<WeaponAPI.WeaponSize, Float> asymetryDebuff = new HashMap<>();
    static {
        asymetryDebuff.put(WeaponAPI.WeaponSize.LARGE,0.2f);
        asymetryDebuff.put(WeaponAPI.WeaponSize.MEDIUM,0.1f);
        asymetryDebuff.put(WeaponAPI.WeaponSize.SMALL,0.05f);
    }
    
    //BEAM OP COST SHENANIGANS
    private final Integer BEAM_COST=3, PD_REFUND =-2;
    
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getDynamic().getMod(Stats.LARGE_BEAM_MOD).modifyFlat(id, BEAM_COST);
        stats.getDynamic().getMod(Stats.MEDIUM_BEAM_MOD).modifyFlat(id, BEAM_COST);
        stats.getDynamic().getMod(Stats.SMALL_BEAM_MOD).modifyFlat(id, BEAM_COST);
        
        stats.getDynamic().getMod(Stats.LARGE_PD_MOD).modifyFlat(id, PD_REFUND);
        stats.getDynamic().getMod(Stats.MEDIUM_PD_MOD).modifyFlat(id, PD_REFUND);
        stats.getDynamic().getMod(Stats.SMALL_PD_MOD).modifyFlat(id, PD_REFUND);
                
        stats.getShieldTurnRateMult().modifyPercent(id, SHIELD_ROTATION);
    }
    
    @Override
    public boolean affectsOPCosts() {
        return true;
    }
    
    private final String ID="Xeno Warfare";
    private final Integer MAX_ASYMETRY=50;
    
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id){
          
        float asymetry=0;
        for(WeaponAPI w : ship.getAllWeapons()){
            if(w.getSlot().isTurret() || w.getSlot().isHardpoint()){
                if(Math.round(w.getSlot().getAngle()) > 1 && Math.round(w.getSlot().getAngle()) < 179){
                    asymetry+=asymetryDebuff.get(w.getSize());
                } else if (Math.round(w.getSlot().getAngle()) < -1 && Math.round(w.getSlot().getAngle()) > -179){                    
                    asymetry-=asymetryDebuff.get(w.getSize());
                }
            }
        }
        if (asymetry!=0){
            float asymetryMult = Math.max(MAX_ASYMETRY/100, 1-Math.abs(asymetry));
            ship.getMutableStats().getMaxSpeed().modifyMult(ID, asymetryMult);
            ship.getMutableStats().getAcceleration().modifyMult(ID, asymetryMult);
            ship.getMutableStats().getTurnAcceleration().modifyMult(ID, asymetryMult);
            ship.getMutableStats().getMaxTurnRate().modifyMult(ID, asymetryMult);            
        }
    }
    
    private final Map <WeaponSize, Integer> weight = new HashMap<>();
    {
        weight.put(WeaponSize.SMALL, 1);
        weight.put(WeaponSize.MEDIUM, 2);
        weight.put(WeaponSize.LARGE, 4);
    }
    
//    @Override
//    public void advanceInCombat(ShipAPI ship, float amount) {
//        if(ship.getAIFlags()!=null){
//            //force broadside
//            if(Math.random()<0.05){
//                Integer side=0;
//                for(WeaponAPI w : ship.getAllWeapons()){
//                    //ignore deco and disabled weapons
//                    if(w.isDecorative() || w.isDisabled() || w.isPermanentlyDisabled()){
//                        continue;
//                    }
//                    //ignore empty weapons
//                    if(w.usesAmmo() && w.getAmmo()<=0){
//                        continue;
//                    }
//                    //ignore missiles
//                    if(w.getType()==WeaponType.MISSILE){
//                        continue;
//                    }
//
//                    WeaponSlotAPI s=w.getSlot();
//
//                    //ignore front and rear facing weapons
//                    if(Math.abs(s.getAngle())<=1 || Math.abs(MathUtils.clampAngle(s.getAngle())-180)<=1){
//                        continue;
//                    }
//                    //add to side
//                    if(MathUtils.clampAngle(s.getAngle())>180){
//                        side+= weight.get(w.getSize());
//                    } else {
//                        side-= weight.get(w.getSize());
//                    }
//                }
//                if(!ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.PREFER_LEFT_BROADSIDE) && !ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.PREFER_RIGHT_BROADSIDE)){
//                    if(side<0){
//                        ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.PREFER_LEFT_BROADSIDE, 2);
//                    } else {
//                         ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.PREFER_RIGHT_BROADSIDE, 2);                            
//                    }
//                }
//            }
//        }
//    }
	
    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {        
        if (index == 0) {
            return MAX_ASYMETRY+"%";
        }
        if (index == 1) {
            return BEAM_COST+" OP";
        }
        if (index == 2) {
            return PD_REFUND+" OP";
        }  
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        // Allows any ship with a ORA hull id
        return ( ship.getHullSpec().getHullId().startsWith("ora_"));	
    }
}

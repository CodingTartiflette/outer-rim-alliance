package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class ORA_CommissionedCrew extends BaseHullMod {
    
    private final float SUPPLIES_ECONOMY = -10;
    private final String CREW="CHM_commission";
    
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getSuppliesPerMonth().modifyPercent(id, SUPPLIES_ECONOMY, "Alliance Commissioned Crew");
    }
	
    //auto remove crew hullmod
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id){
        if (ship.getVariant().getHullMods().contains(CREW)) {                
            ship.getVariant().removeMod(CREW);      
        }        
    }
    
    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {        
        if (index == 0) {
            return SUPPLIES_ECONOMY+"%";
        }
        return null;
    }
}

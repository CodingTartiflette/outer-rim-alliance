package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;

public class ORA_solid extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {            
        stats.getShieldDamageTakenMult().modifyMult(id, 1f - 0.66f * effectLevel);		
        stats.getShieldUpkeepMult().modifyMult(id, 1f - 0.66f * effectLevel);
        
        ShipAPI ship = (ShipAPI) stats.getEntity();
        if(ship!=null && ship.getFluxTracker()!= null){
            float softFlux = ship.getFluxTracker().getCurrFlux()-ship.getFluxTracker().getHardFlux();
            softFlux = ship.getFluxTracker().getHardFlux()+softFlux/3;
            ship.getFluxTracker().setCurrFlux(softFlux);        
            ship.getFluxTracker().setHardFlux(softFlux);
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getShieldDamageTakenMult().unmodify(id);
        stats.getShieldUpkeepMult().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
                return new StatusData("3:1 Soft flux conversion to Hard flux.", false);
        }
        else if (index == 1) {
                return new StatusData("Shield strength increased.", false);
        }
        return null;
    }
}
